const deepClone = object => {
  return JSON.parse(JSON.stringify(object));
};

const randomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

const addMember = (data, newMember, memberData = {}) => {
  let newData = deepClone(data);
  newData.push({ name: newMember, data: memberData });
  return newData;
};

const gossip = (data, randomiser = randomInt) => {
  const dataSize = data.length;
  let newData = deepClone(data);
  let currentMember;
  let randomMember;
  let mergedData;
  for (index = 0; index < dataSize; index++) {
    currentMember = newData[index];
    randomMember = newData[randomiser(dataSize)];
    mergedData = Object.assign({}, currentMember.data, randomMember.data);
    currentMember.data = mergedData;
    randomMember.data = mergedData;
  }
  return newData;
};

exports.addMember = addMember;
exports.gossip = gossip;
