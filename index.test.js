const { addMember, gossip } = require("./index");

describe("members", () => {
  test("add a member", () => {
    const data = [];

    expect(addMember(data, "Alice")).toEqual([{ name: "Alice", data: {} }]);
  });
});

describe("gossip", () => {
  test("spread data", () => {
    const data1 = [
      { name: "Alice", data: { "8a2d4686": "transaction" } },
      { name: "Bob", data: { ce740668: "transaction" } },
      { name: "Dave", data: { d583274a: "transaction" } }
    ];

    let count = 0;
    const mockRandomiser = max => {
      return ++count % data1.length;
    };

    let data2 = gossip(data1, mockRandomiser);

    expect(data2).toEqual([
      {
        data: {
          "8a2d4686": "transaction",
          ce740668: "transaction",
          d583274a: "transaction"
        },
        name: "Alice"
      },
      {
        data: {
          "8a2d4686": "transaction",
          ce740668: "transaction",
          d583274a: "transaction"
        },
        name: "Bob"
      },
      {
        data: {
          "8a2d4686": "transaction",
          ce740668: "transaction",
          d583274a: "transaction"
        },
        name: "Dave"
      }
    ]);
  });
});
